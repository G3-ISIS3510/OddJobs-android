package com.piazuelo.oddjobs.helpers;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.fragments.HistoryEmployeeFragment;
import com.piazuelo.oddjobs.fragments.HistoryEmployerFragment;
import com.piazuelo.oddjobs.fragments.MyJobsFragment;
import com.piazuelo.oddjobs.fragments.PendingEmployeeFragment;
import com.piazuelo.oddjobs.fragments.PendingEmployerFragment;
import com.piazuelo.oddjobs.fragments.ShowJobsFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ngomez on 11/12/17.
 */

public class FirebaseHelper {

    private static final String TAG = "FirebaseHelper";
    public static final String NO_EMPLOYEE = "NO_ASSIGNED_YET";
    public static final String FINISHED = "FINISHED";

    private DatabaseReference mDatabase;
    private ArrayList<Job> jobs;
    private HashMap<Job, String> map;

    private ArrayList<Job> employerAccepted;
    private ArrayList<Job> employerPending;
    private ArrayList<Job> employerHistory;
    private ArrayList<Job> employeeSearch;
    private ArrayList<Job> employeePending;
    private ArrayList<Job> employeeHistory;

    public FirebaseHelper() {
        jobs = new ArrayList<>();
        map = new HashMap<>();
        employerAccepted = new ArrayList<>();
        employerPending = new ArrayList<>();
        employerHistory = new ArrayList<>();
        employeeSearch = new ArrayList<>();
        employeePending = new ArrayList<>();
        employeeHistory = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("jobs")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Job j = dataSnapshot.getValue(Job.class);
                        String jobKey = dataSnapshot.getKey().toString();
                        processJob(j, jobKey);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Job j = dataSnapshot.getValue(Job.class);
                        String jobKey = dataSnapshot.getKey().toString();
                        String assignedEmployee = j.getEmployeeId();
                        if(j.getType().equals(FINISHED)) {
                            Log.d(TAG, jobKey + " modified");
                            remove(j);
                            j.setEmployeeId(assignedEmployee);
                            processJob(j, jobKey);
                        }
                        else if(!assignedEmployee.equals(NO_EMPLOYEE)) {
                            Log.d(TAG, jobKey + " modified");
                            j.setEmployeeId(NO_EMPLOYEE);
                            remove(j);
                            j.setEmployeeId(assignedEmployee);
                            processJob(j, jobKey);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Job j = dataSnapshot.getValue(Job.class);
                        jobs.remove(j);
                        map.remove(j);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private void processJob(Job j, String jobKey) {
        jobs.add(j);
        map.put(j, jobKey);

        if(j.getUserId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            if(j.getDate() < System.currentTimeMillis()) {
                employerHistory.add(j);
                HistoryEmployerFragment.dataChanged();
            } else {
                if(!j.getEmployeeId().equals(NO_EMPLOYEE)) {
                    employerAccepted.add(j);
                    MyJobsFragment.dataChanged();
                } else {
                    employerPending.add(j);
                    PendingEmployerFragment.dataChanged();
                }
            }
        } else {
            if(j.getDate() > System.currentTimeMillis()) {
                if(j.getEmployeeId().equals(NO_EMPLOYEE)) {
                    employeeSearch.add(j);
                    ShowJobsFragment.dataChanged();
                } else {
                    if(j.getEmployeeId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        employeePending.add(j);
                        PendingEmployeeFragment.dataChanged();
                    }
                }
            } else {
                if(j.getEmployeeId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    employeeHistory.add(j);
                    HistoryEmployeeFragment.dataChanged();
                }
            }
        }
    }
    private void remove(Job j) {
        map.remove(j);
        jobs.remove(j);
        if(employerPending.remove(j)) {
            Log.d(TAG, "Lo borroooo de employerPending");
            PendingEmployerFragment.dataChanged();
        }
        if(employeeSearch.remove(j)) {
            Log.d(TAG, "Lo borroooo de employeeSearch");
            ShowJobsFragment.dataChanged();
        }
        if(employerHistory.remove(j)) {
            Log.d(TAG, "Lo borroooo de employerHistory");
            HistoryEmployerFragment.dataChanged();
        }
    }

    public String getJobKey(Job j) {
        return map.get(j);
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public List<Job> getJobSearch() {
        return employeeSearch;
    }

    public List<Job> getEmployeeHistory() {
        return employeeHistory;
    }

    public List<Job> getEmployeePending() {
        return employeePending;
    }

    public List<Job> getEmployerHistory() {
        return employerHistory;
    }

    public List<Job> getEmployerPending() {
        return employerPending;
    }

    public List<Job> getEmployerAccepted() {
        return employerAccepted;
    }
}
