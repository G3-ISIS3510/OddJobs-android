package com.piazuelo.oddjobs.helpers;

import java.text.NumberFormat;

/**
 * Created by ngomez on 10/5/17.
 */

public class InputFormat {

    public static final String URL_REGEX = "^(https?|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    public static final String PASSWORD_REGEX = "^(?=.*[A-z])(?=.*[0-9])[A-z0-9]+$";

    public static boolean validPassword(String pw) {
        if(pw.length() >= 8 || pw.matches(PASSWORD_REGEX)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean passwordsMatch(String pw1, String pw2) {
        return pw1.equals(pw2);
    }

    public static boolean isLink(String link) {
        return link.matches(URL_REGEX);
    }

    public static String formatCurrency(double amount) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(amount);
    }
}
