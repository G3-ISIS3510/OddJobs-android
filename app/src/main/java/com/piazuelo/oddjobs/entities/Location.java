package com.piazuelo.oddjobs.entities;

/**
 * Created by ngomez on 10/7/17.
 */

public class Location {
    private String latitude;
    private String longitude;

    public Location() {
        latitude = "";
        longitude = "";
    }

    public Location(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object obj) {
        Location l = (Location) obj;
        return latitude.equals(l.getLatitude())
                && longitude.equals(l.getLongitude());
    }
}
