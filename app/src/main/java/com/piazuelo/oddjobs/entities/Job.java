package com.piazuelo.oddjobs.entities;


import android.os.Parcel;
import android.os.Parcelable;

public class Job implements Parcelable{

    // JOb Table Columns
    public static final String TABLE_NAME = "jobs";
    public static final String KEY_JOB_ID = "id";

    public static final String KEY_JOB_NAME= "name";
    public static final String KEY_JOB_DESCRIPTION = "description";
    public static final String KEY_JOB_REWARD = "reward";
    public static final String KEY_JOB_LOCATION = "location";
    public static final String KEY_JOB_ADDRESS = "address";
    public static final String KEY_JOB_IMAGE = "image";
    public static final String KEY_JOB_USER_ID = "userId";
    public static final String KEY_JOB_TYPE = "type";
    public static final String KEY_JOB_DATE = "date";
    public static final String KEY_JOB_EMPLOYEE_ID = "employeeId";

    //********************************

    private String name;
    private long id;
    private String description;
    private double reward;
    private Location location;
    private String address;
    private String image;
    private String userId;
    private String type;
    private long date;
    private String employeeId;

    public Job() {
    }

    public Job(String name, String description, double reward, Location loc, String address, String image, String userId, String type, long date, String employeeId) {
        this.name = name;
        this.date = date;
        this.description = description;
        this.reward = reward;
        this.location = loc;
        this.address = address;
        this.image = image;
        this.userId = userId;
        this.type = type;
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getReward() {return reward;}

    public Location getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }

    public String getImage() {
        return image;
    }

    public String getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public long getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeDouble(reward);
        parcel.writeString(location.getLatitude());
        parcel.writeString(location.getLongitude());
        parcel.writeString(address);
        parcel.writeString(image);
        parcel.writeString(userId);
        parcel.writeString(type);
        parcel.writeLong(date);
        parcel.writeString(employeeId);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Job> CREATOR = new Parcelable.Creator<Job>() {
        public Job createFromParcel(Parcel in) {
            return new Job(
                    in.readString(),
                    in.readString(),
                    in.readDouble(),
                    new Location(in.readString(), in.readString()),
                    in.readString(),
                    in.readString(),
                    in.readString(),
                    in.readString(),
                    in.readLong(),
                    in.readString());
        }

        public Job[] newArray(int size) {
            return new Job[size];
        }
    };

    public void setId(long id) {
        this.id = id;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object obj) {
        Job j = (Job) obj;
        return name.equals(j.name)
                && description.equals(j.description)
                && reward == j.reward
                && location.equals(j.location)
                && address.equals(j.address)
                && image.equals(j.getImage())
                && userId.equals(j.getUserId())
                && employeeId.equals(j.employeeId);
    }
}
