package com.piazuelo.oddjobs.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jgtamura on 10/9/17.
 */

public class User implements Parcelable {
    private String name;
    private String cv;
    private double rating;
    private int reviews;
    public User(){
    }

    public User(String name,String cv, double rating, int reviews){
        this.name=name;
        this.cv=cv;
        this.rating=rating;
        this.reviews=reviews;

    }

    public String getName() {
        return name;
    }

    public String getCv() {
        return cv;
    }

    public double getRating() {
        return rating;
    }

    public int getReviews() {
        return reviews;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeString(cv);
        parcel.writeDouble(rating);
        parcel.writeInt(reviews);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(
                    in.readString(),
                    in.readString(),
                    in.readDouble(),
                    in.readInt());
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
