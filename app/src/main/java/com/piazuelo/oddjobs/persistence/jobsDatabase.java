package com.piazuelo.oddjobs.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.entities.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgtamura on 11/7/17.
 */

public class jobsDatabase extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Job.TABLE_NAME + " (" +
                    Job.KEY_JOB_ID + " INTEGER PRIMARY KEY," +
                    Job.KEY_JOB_NAME + " TEXT," +

                    Job.KEY_JOB_DESCRIPTION+ " TEXT," +
                    Job.KEY_JOB_REWARD + " INTEGER,"+
                    Job.KEY_JOB_LOCATION + " TEXT," +
                    Job.KEY_JOB_ADDRESS + " TEXT," +
                    Job.KEY_JOB_IMAGE + " TEXT," +
                    Job.KEY_JOB_USER_ID + " TEXT," +
                    Job.KEY_JOB_TYPE + " TEXT," +
                    Job.KEY_JOB_DATE + " INTEGER,"+
                    Job.KEY_JOB_EMPLOYEE_ID + " TEXT)" ;

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Job.TABLE_NAME;


    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Jobs.db";

    public jobsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void deleteJobs(){
        // Create and/or open the database for writing
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("delete from "+ Job.TABLE_NAME);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("DB", "Error while trying to add post to database");
        } finally {

            db.endTransaction();
        }
    }

    public void addJob(final Job job) {

        // Create and/or open the database for writing
        SQLiteDatabase db = getWritableDatabase();

        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        db.beginTransaction();
        try {
            // The user might already exist in the database (i.e. the same user created multiple posts).

            ContentValues values = new ContentValues();
            values.put(Job.KEY_JOB_NAME, job.getName());

            values.put(Job.KEY_JOB_DESCRIPTION,  job.getDescription());
            values.put(Job.KEY_JOB_REWARD ,job.getReward()+"");
            values.put(Job.KEY_JOB_LOCATION,job.getLocation().getLatitude()+"-"+job.getLocation().getLongitude());
            values.put(Job.KEY_JOB_ADDRESS ,job.getAddress());
            values.put(Job.KEY_JOB_IMAGE ,job.getImage());
            values.put(Job.KEY_JOB_USER_ID ,job.getUserId());
            values.put(Job.KEY_JOB_TYPE ,job.getType());
            values.put(Job.KEY_JOB_DATE ,job.getDate()+"");
            values.put(Job.KEY_JOB_EMPLOYEE_ID ,job.getEmployeeId());


            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            final long jobId =db.insertOrThrow(Job.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
            job.setId(jobId);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("DB", "Error while trying to add post to database");
        } finally {

            db.endTransaction();
        }
    }
    public List<Job> getAllJobs() {
        List<Job> jobs = new ArrayList<>();
        String JOB_QUERY =
                String.format("SELECT * FROM %s ",
                        Job.TABLE_NAME);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(JOB_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {

                    long jobId = cursor.getLong(cursor.getColumnIndex(Job.KEY_JOB_ID));
                    String name = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_NAME));

                    String description = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_DESCRIPTION));
                    String address = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_ADDRESS));
                    String image = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_IMAGE));
                    String type = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_TYPE));
                    String employeeId = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_EMPLOYEE_ID));
                    String userId = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_USER_ID));
                    String date = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_DATE));

                    String[] latLong = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_LOCATION)).split("-");
                    String reward = cursor.getString(cursor.getColumnIndex(Job.KEY_JOB_REWARD));
                    Location loc = new Location(latLong[0],latLong[1]);

                    Job newJob = new Job(name,description,Double.parseDouble(reward),loc,address,image,userId,type,Long.parseLong(date),employeeId);

                    jobs.add(newJob);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("DB", "Error while trying to get populations from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return jobs;
    }


}
