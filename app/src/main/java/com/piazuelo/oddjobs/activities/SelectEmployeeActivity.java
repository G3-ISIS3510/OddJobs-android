package com.piazuelo.oddjobs.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.adapters.EmployeeAdapter;
import com.piazuelo.oddjobs.entities.Job;

import java.util.ArrayList;
import java.util.List;

public class SelectEmployeeActivity extends AppCompatActivity {

    /**
     * Firebase Database
     */
    private DatabaseReference mDatabase;
    private ArrayList<String> candidates;

    /**
     * UI components
     */
    private ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_employee);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        createListOfEmployees();
    }

    public void createListOfEmployees(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        candidates = new ArrayList<>();

        lv = findViewById(R.id.listViewMyEmployees);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Intent i = getIntent();
        if(i != null) {
            final String key = i.getStringExtra("jobId");
            mDatabase.getDatabase().getReference("candidates").child(key).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    GenericTypeIndicator<List<String>> t = new GenericTypeIndicator<List<String>>() {};
                    candidates = (ArrayList<String>) dataSnapshot.getValue(t);
                    printToScreen();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }
    public  void printToScreen(){
            EmployeeAdapter adapter = new EmployeeAdapter(this, candidates);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String candidateSelected = candidates.get(i);
                    Intent intent = getIntent();
                    if(intent != null) {
                        String key = intent.getStringExtra("jobId");
                        mDatabase.child("jobs").child(key).child("employeeId").setValue(candidateSelected);
                        Toast.makeText(SelectEmployeeActivity.this, "Success! You have a employee!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
