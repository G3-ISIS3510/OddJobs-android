package com.piazuelo.oddjobs.activities;

import android.content.DialogInterface;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;

import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.entities.Job;

import java.util.ArrayList;

import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.helpers.InputFormat;

import java.util.Calendar;
import java.util.List;

public class JobDetailsActivity extends AppCompatActivity  {

    public static final String TAG = "JOB_DETAILS";

    private DatabaseReference mDatabase;
    private List candidates;
    private String key = "";

    /**
     * UI components
     */
    private TextView userTxt;
    private Button takeJobBtn;
    private TextView ratingTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userTxt = findViewById(R.id.jobDetailsUserTxt);
        takeJobBtn = findViewById(R.id.jobDetailsTakeBtn);
        ratingTxt = findViewById(R.id.jobDetailsRatingTxt);

        TextView nameTxt = findViewById(R.id.jobDetailsNameTxt);
        TextView descriptionTxt = findViewById(R.id.jobDetailsDescriptionTxt);
        TextView rewardTxt = findViewById(R.id.jobDetailsRewardTxt);
        TextView addressTxt = findViewById(R.id.jobDetailsAddressTxt);
        TextView dateTxt = findViewById(R.id.jobDetailsDateTxt);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        Intent i = getIntent();
        if(i != null) {
            final Job j = i.getParcelableExtra("job");
            key = i.getStringExtra("key");

            mDatabase.getDatabase().getReference("candidates").child(key).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    GenericTypeIndicator<List<String>> t = new GenericTypeIndicator<List<String>>() {};
                    candidates = dataSnapshot.getValue(t);
                    if(candidates != null && candidates.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        if(j.getEmployeeId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            takeJobBtn.setText(R.string.findEmployer);
                            takeJobBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    authWithEmployer((String) userTxt.getText(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                                }
                            });
                        }
                        else{
                            takeJobBtn.setEnabled(true);
                            takeJobBtn.setText(R.string.waitingForReply);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            WebView jobImg = findViewById(R.id.jobDetailsImgView);
            if(ConnectivityHelper.isOnline(getApplicationContext()))
                jobImg.loadUrl(j.getImage());
            else
                jobImg.loadUrl("file:///android_asset/nointernet.html");

            jobImg.getSettings().setLoadWithOverviewMode(true);
            jobImg.getSettings().setUseWideViewPort(true);
            jobImg.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);


            String nameAndType = j.getType() + ": " + j.getName();
            nameTxt.setText(nameAndType);
            descriptionTxt.setText(j.getDescription());

            mDatabase.child("users").child(j.getUserId()).child("name").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String uName = dataSnapshot.getValue(String.class);
                    userTxt.setText(uName == null || uName.equals("") ? "No name provided" : uName);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });

            mDatabase.child("users").child(j.getUserId()).child("rating").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Double uRating = dataSnapshot.getValue(Double.class);
                    String ratingText = "" + uRating;
                    ratingTxt.setText(ratingText);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });

            String reward = InputFormat.formatCurrency(j.getReward());
            rewardTxt.setText(reward.split("\\.")[0]);
            addressTxt.setText(j.getAddress());

            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(j.getDate());
            String dateToShow = date.get(date.DAY_OF_MONTH)+"/"+
                    (date.get(date.MONTH)+1)+"/"+
                    date.get(date.YEAR)+" "+
                    showDigit(date.get(date.HOUR))+":"+
                    showDigit(date.get(date.MINUTE));
            dateTxt.setText(dateToShow);

            if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(j.getUserId())) {
                if(j.getEmployeeId().equals("NO_ASSIGNED_YET") && j.getDate() < System.currentTimeMillis()) {
                    takeJobBtn.setText(R.string.nooneHelped);
                }
                else if(j.getEmployeeId().equals("NO_ASSIGNED_YET")) {

                    takeJobBtn.setText(R.string.chooseEmployee);
                    takeJobBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            chooseEmployee(key);
                        }
                    });

                }
                else if(j.getDate() > System.currentTimeMillis()){
                    mDatabase.child("users").child(j.getEmployeeId()).child("name").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final String name = dataSnapshot.getValue(String.class);
                            takeJobBtn.setText(String.format(getString(R.string.contactEmployee), name));
                            takeJobBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    contactEmployee(name);
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });


                }
                else {

                    mDatabase.child("users").child(j.getEmployeeId()).child("name").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String name = dataSnapshot.getValue(String.class);

                            takeJobBtn.setText(String.format(getString(R.string.rateEmployee), name));
                            takeJobBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(JobDetailsActivity.this);
                                    builder.setTitle("Rate");

                                    // Set up the input
                                    final EditText input = new EditText(JobDetailsActivity.this);
                                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                    input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                                    builder.setView(input);

                                    // Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try{
                                                final long rate = Integer.parseInt(input.getText().toString());

                                                mDatabase.child("users").child(j.getEmployeeId()).child("rating").addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        final Long rating = dataSnapshot.getValue(Long.class);

                                                        mDatabase.child("users").child(j.getEmployeeId()).child("reviews").addValueEventListener(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                Long reviews = dataSnapshot.getValue(Long.class);
                                                                long newRate = 5;
                                                                if(reviews > 0){
                                                                    newRate = (rating+rate)/reviews;
                                                                }
                                                                else{
                                                                    newRate = (rating+rate)/2;
                                                                }

                                                                mDatabase.getDatabase().getReference("users").child(key).child("rating").setValue(newRate);
                                                                mDatabase.getDatabase().getReference("users").child(key).child("reviews").setValue(reviews+1);
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {}
                                                        });


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {}
                                                });

                                            }
                                            catch (Exception e){
                                                Toast.makeText(JobDetailsActivity.this, "Sorry, your input is not a valid number", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });


                }
            } else {
                if(j.getEmployeeId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    takeJobBtn.setText(R.string.findEmployer);
                    takeJobBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            authWithEmployer((String)userTxt.getText(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                        }
                    });
                } else if(j.getEmployeeId().equals("NO_ASSIGNED_YET")) {
                    takeJobBtn.setText(R.string.takeJobBtn);
                    takeJobBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            volunteerAsEmployee(key);
                        }
                    });
                }
            }
        }
    }

    public String showDigit(int digit){
        String digitS = digit+"";
        if(digitS.length() == 1){
            return "0"+digitS;
        }
        return digitS;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void volunteerAsEmployee(final String key) {
        if(!ConnectivityHelper.isOnline(this)){

            new AlertDialog.Builder(this)
                    .setTitle("No internet")
                    .setMessage("You can't postulate to a job if you are offline")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .create().show();
            return;
        }
        if(candidates == null){
            candidates = new ArrayList();
        }
        if(!candidates.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            candidates.add(FirebaseAuth.getInstance().getCurrentUser().getUid());
            mDatabase.getDatabase().getReference("candidates").child(key).setValue(candidates);
            Toast.makeText(JobDetailsActivity.this, R.string.waitForReply, Toast.LENGTH_LONG).show();
        }
    }

    private void chooseEmployee(final String key) {
        if(!ConnectivityHelper.isOnline(this)){

            new AlertDialog.Builder(this)
                    .setTitle("No internet")
                    .setMessage("You can't choose an employee if you are offline.")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .create().show();
            return;
        }
        if(candidates == null) {
            new AlertDialog.Builder(this)
                    .setTitle("No candidates yet!")
                    .setMessage(R.string.noEmployeesYet)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .create().show();
        }
        else{
            Intent i = new Intent(this, SelectEmployeeActivity.class);
            i.putExtra("jobId", key);
            startActivity(i);
        }
    }
    private void contactEmployee(final String key) {
        Intent i = new Intent(this, BeaconEmployerAuthActivity.class);
        i.putExtra("employee", key);
        startActivity(i);

    }
    private void authWithEmployer(final String key, final String employee) {
        Intent i = new Intent(this, BeaconEmployeeAuthActivity.class);
        i.putExtra("employer", key);
        i.putExtra("employee", employee);
        startActivity(i);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
