package com.piazuelo.oddjobs.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.piazuelo.oddjobs.R;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BeaconTransmitter;

import java.util.ArrayList;
import java.util.Arrays;


public class BeaconEmployeeAuthActivity extends AppCompatActivity  {
    private String employee;
    private Button stop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_beacon_employee_auth_actitivy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String employer = "";
        employee = "";
        if(getIntent() != null && getIntent().hasExtra("employer")) {
            employer = getIntent().getStringExtra("employer");
        }
        if(getIntent() != null && getIntent().hasExtra("employee")) {
            employee = getIntent().getStringExtra("employee");
        }

        TextView title = findViewById(R.id.EmployeeAuthEmployee);
        title.setText("Your device is making the authentication with "+employer+". Please be patient" );

        stop = findViewById(R.id.buttonStopAuth);
        stop.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createLoader();
            }
        });
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            stop.setText("You cant authenticate");
            new AlertDialog.Builder(this)
                    .setTitle("Sorry")
                    .setMessage("The authentication needs bluetooth to work and your device don't support bluetooth.")
                    .setNegativeButton("Ok", null)
                    .create().show();
        } else if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enable :)
                stop.setText("Enable your bluetooth first and try again.");
                new AlertDialog.Builder(this)
                        .setTitle("Enable bluetooth")
                        .setMessage("The authentication needs bluetooth to work.")
                        .setNegativeButton("Ok", null)
                        .create().show();
        } else if (mBluetoothAdapter.isEnabled()){
            createLoader();
        }






    }
    public void createLoader(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            new AlertDialog.Builder(this)
                    .setTitle("Enable bluetooth")
                    .setMessage("The authentication needs bluetooth to work.")
                    .setNegativeButton("Ok", null)
                    .create().show();
        }
        else{
            stop.setText("authenticating...");
            ArrayList<Long> array = new ArrayList<>();
            byte [] bytes = employee.getBytes();
            System.out.println(Arrays.toString(bytes));
            for(int i=0;i< bytes.length;i++){
                array.add( (Long.parseLong(bytes[i]+"") ));
            }
            Beacon beacon = new Beacon.Builder()
                    .setId1("2f234454-cf6d-4a0f-adf2-f4911ba9ffa6")
                    //employee
                    .setId2("1")
                    .setId3("2")
                    .setManufacturer(0x0117)
                    .setTxPower(-59)
                    .setDataFields(array)
                    .build();

            BeaconParser beaconParser = new BeaconParser()
                    .setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
            BeaconTransmitter beaconTransmitter = new BeaconTransmitter(getApplicationContext(), beaconParser);

            beaconTransmitter.startAdvertising(beacon);

        }


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}




