package com.piazuelo.oddjobs.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.piazuelo.oddjobs.R;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public class BeaconEmployerAuthActivity extends AppCompatActivity implements BeaconConsumer {

    private String employee;
    private TextView status;
    private TextView proximity;

    private Button  buttonStopAuthEmployer;

    private BeaconManager beaconManager;

    private Context mainC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainC = this;
        setContentView(R.layout.activity_employer_auth);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        employee = "";
        if(getIntent() != null && getIntent().hasExtra("employee")) {
            employee = getIntent().getStringExtra("employee");
        }
        TextView title = findViewById(R.id.EmployerAuthEmployee);
        title.setText(employee);

        status = findViewById(R.id.EmployerAuthStatus);
        proximity = findViewById(R.id.EmployerAuthProximity);

        buttonStopAuthEmployer = findViewById(R.id.buttonStopAuthEmployer);
        buttonStopAuthEmployer.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                beaconStart();
            }
        });

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            buttonStopAuthEmployer.setText("You cant authenticate");
            status.setText("Failed");
            proximity.setText("No data");
            new AlertDialog.Builder(this)
                    .setTitle("Sorry")
                    .setMessage("The authentication needs bluetooth to work and your device don't support bluetooth.")
                    .setNegativeButton("Ok", null)
                    .create().show();
        } else if (!mBluetoothAdapter.isEnabled()) {
            // Bluetooth is not enable :)
            buttonStopAuthEmployer.setText("Enable your bluetooth first and try again.");
            status.setText("Failed");
            proximity.setText("No data");
            new AlertDialog.Builder(this)
                    .setTitle("Enable bluetooth")
                    .setMessage("The authentication needs bluetooth to work.")
                    .setNegativeButton("Ok", null)
                    .create().show();
        } else if (mBluetoothAdapter.isEnabled()){
            beaconStart();
        }

    }
    public void beaconStart(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            // Bluetooth is not enable :)
            status.setText("Failed");
            proximity.setText("No data");
            buttonStopAuthEmployer.setText("Enable your bluetooth first and try again.");
            new AlertDialog.Builder(this)
                    .setTitle("Enable bluetooth")
                    .setMessage("The authentication needs bluetooth to work.")
                    .setNegativeButton("Ok", null)
                    .create().show();
        }
        else{
            status.setText("Searching...");
            proximity.setText("Searching...");
            buttonStopAuthEmployer.setText("Searching ...");
            beaconManager = BeaconManager.getInstanceForApplication(this);
            // To detect proprietary beacons, you must add a line like below corresponding to your beacon
            // type.  Do a web search for "setBeaconLayout" to get the proper expression.
            // beaconManager.getBeaconParsers().add(new BeaconParser().
            //        setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
            beaconManager.bind(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(beaconManager != null){
            beaconManager.unbind(this);
        }

    }
    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<org.altbeacon.beacon.Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    Iterator beaconIt = beacons.iterator();
                    for(org.altbeacon.beacon.Beacon current : beacons) {
                        List data = current.getDataFields();
                        byte [] bytes = new byte[data.size()];
                        for(int i=0; i<data.size();i++){
                            Long value = (Long)data.get(i);

                            bytes[i] = value.byteValue();
                        }
                        String employeeData = new String(bytes);
                        Log.d("BLUETOOTH", employeeData);
                        if(employee.startsWith(employeeData)){
                            //Log.d("BLUETOOTH", "Match found");
                            //Toast.makeText(BeaconEmployerAuthActivity.this, "Employee found!", Toast.LENGTH_LONG).show();
                            //Log.d("BLUETOOTH", "Toast made supuestamente");
                            updateUI("" + current.getDistance());
                            break;
                        }

                    }
                }

            }
        } );

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (Exception e) {    }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void updateUI(final String distance) {
        beaconManager.unbind(this);

      //  new AlertDialog.Builder(mainC)
        //        .setTitle("FOUND!")
          //      .setMessage("Success! Your employee is close by (" + distance + ")")
            //    .setNegativeButton("Ok", null)
              //  .create().show();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Success! Your employee is right next to you.");
                status.setTextSize(20);
                proximity.setText("Your employee is " + distance + " meters from you.");
                proximity.setTextSize(20);
                //stuff that updates ui
                buttonStopAuthEmployer.setText("Authenticated");
                buttonStopAuthEmployer.setClickable(false);

            }
        });
    }



}




