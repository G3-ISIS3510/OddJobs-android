package com.piazuelo.oddjobs.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.helpers.ImageHelper;
import com.piazuelo.oddjobs.helpers.InputFormat;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = "PROFILE";
    public static final int REQUEST_PHOTO = 1;
    public static final int REQUEST_PERMISSION = 2;

    private DatabaseReference mDatabase;

    /**
     * UI components
     */
    private ImageView pic;
    private EditText name;
    private EditText cv;
    private Uri imgToUpload;
    private EditText currentPassword;
    private EditText newPassword1;
    private EditText newPassword2;
    private ProgressDialog loadingDialog;


    /**
     * Currently logged in user
     */
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pic = findViewById(R.id.userImgView);
        name = findViewById(R.id.userNameEditTxt);
        cv = findViewById(R.id.userCVEditTxt);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            toLogin();
            return;
        }

        String uName = user.getDisplayName();
        imgToUpload = user.getPhotoUrl();

        mDatabase.child("users").child(user.getUid()).child("cv").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String uCV = dataSnapshot.getValue(String.class);
                cv.setText(uCV);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        if (uName != null && !uName.isEmpty()) {
            name.setText(uName);
        } else {
            name.setHint(R.string.nameHint);
        }

        if (imgToUpload != null) {
            Picasso.with(this)
                    .load(imgToUpload)
                    .resize(170, 170)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(pic);
        } else {
            // pic.setImageBitmap(ImageHelper.decodeSampledBitmapFromResource(getResources(), R.drawable.default_avatar, 200, 200));
            Picasso.with(this)
                    .load(R.drawable.male_icon)
                    .resize(170, 170)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(pic);
        }

        currentPassword = findViewById(R.id.currentPasswordEditTxt);
        newPassword1 = findViewById(R.id.changePassword1EditTxt);
        newPassword2 = findViewById(R.id.changePassword2EditTxt);

        ImageButton chooseImg = findViewById(R.id.chooseImgBtn);
        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImg();
            }
        });

        Button saveName = findViewById(R.id.saveNameAndImgBtn);
        saveName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNameAndImg(view);
            }
        });

        Button savePassword = findViewById(R.id.updatePasswordBtn);
        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePassword();
            }
        });
    }

    private String extractText(EditText v) {
        return v.getText() != null ? v.getText().toString().trim() : "";
    }

    private void chooseImg() {
        PackageManager pm = getPackageManager();
        if(!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, R.string.noCamera, Toast.LENGTH_LONG).show();
            return;
        }
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "oddjobs/avatar.png");
        takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        String pickTitle = getString(R.string.imagePickerTitle);
        Intent chooser = Intent.createChooser(pickIntent, pickTitle);
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { takePicIntent });
        imgToUpload = Uri.fromFile(f);
        if(takePicIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(chooser, REQUEST_PHOTO);
        }
    }

    private void saveNameAndImg(final View v) {
        String newName = extractText(name);
        String newCV = extractText(cv);

        if(newName.isEmpty()) {
           Toast.makeText(this, R.string.noNameProvided, Toast.LENGTH_LONG).show();
            return;
        }
        if(!newCV.equals("") && !InputFormat.isLink(newCV)) {
            Toast.makeText(this, R.string.cvNotLink, Toast.LENGTH_LONG).show();
            return;
        }

        loadingDialog = new ProgressDialog(ProfileActivity.this);
        loadingDialog.setMessage(getString(R.string.updatingProfile));
        loadingDialog.setTitle(R.string.loading);
        loadingDialog.show();

        FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(newName)
                .setPhotoUri(imgToUpload)
                .build();

        loggedUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                            Toast.makeText(ProfileActivity.this, R.string.success, Toast.LENGTH_SHORT).show();
                            // Move to next activity
                            Intent i = new Intent(v.getContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(ProfileActivity.this, R.string.somethingWrong, Toast.LENGTH_SHORT).show();
                        }

                        if(loadingDialog != null){
                            loadingDialog.dismiss();
                        }
                    }
                });

        mDatabase.child("users").child(loggedUser.getUid()).child("cv").setValue(newCV);
        mDatabase.child("users").child(loggedUser.getUid()).child("name").setValue(newName);
    }

    private void savePassword() {
        final String newPass = extractText(newPassword2);
        final String newPass2 = extractText(newPassword1);

        if(newPass.equals("")) {
            Toast.makeText(this, R.string.noPasswordProvided, Toast.LENGTH_LONG).show();
            return;
        }
        if(!InputFormat.validPassword(newPass)) {
            Toast.makeText(this, R.string.invalidPassword, Toast.LENGTH_LONG).show();
            return;
        }
        if(!InputFormat.passwordsMatch(newPass, newPass2)) {
            Toast.makeText(this, R.string.differentPasswords, Toast.LENGTH_LONG).show();
            return;
        }

        loadingDialog = new ProgressDialog(ProfileActivity.this);
        loadingDialog.setMessage(getString(R.string.updatingProfile));
        loadingDialog.setTitle(R.string.loading);
        loadingDialog.show();

        final FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();

        // Get auth credentials from the user for re-authentication. The example below shows
        // email and password credentials but there are multiple possible providers,
        // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider
                .getCredential(loggedUser.getEmail(), extractText(currentPassword));

        // Prompt the user to re-provide their sign-in credentials
        loggedUser.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            loggedUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(ProfileActivity.this, R.string.passwordUpdated, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(ProfileActivity.this, R.string.passwordNotUpdated, Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } else {
                            if(ConnectivityHelper.isOnline(getApplicationContext())) {
                                Toast.makeText(ProfileActivity.this, R.string.wrongPassword, Toast.LENGTH_LONG).show();
                                Log.d(TAG, task.getException().getMessage());
                            } else {
                                Toast.makeText(ProfileActivity.this, R.string.somethingWrong, Toast.LENGTH_LONG).show();
                            }

                        }
                        if(loadingDialog != null){
                            loadingDialog.dismiss();
                        }
                    }
                });
    }

    private void toLogin() {
        Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Llegó el resultado de la actividad");
        if (requestCode == REQUEST_PHOTO && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Log.d(TAG, "El resultado no era null");
                imgToUpload = data.getData();
            }
            if (imgToUpload != null) {
                Uri selectedImage = imgToUpload;
                getContentResolver().notifyChange(selectedImage, null);
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_PERMISSION);
                    Log.d(TAG, "No permission");
                }
                ContentResolver cr = getContentResolver();
                Bitmap reducedSizeBitmap = ImageHelper.getBitmap(cr, ImageHelper.getRealPathFromURI(cr, imgToUpload));
                if (reducedSizeBitmap != null) {
                    Log.d(TAG, "Image received");
                    Picasso.with(this)
                            .load(imgToUpload)
                            .resize(170, 170)
                            .centerCrop()
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(pic);
                }
            } else {
                Toast.makeText(this, R.string.imageUriError, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
