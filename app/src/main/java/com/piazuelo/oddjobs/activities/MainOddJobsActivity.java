package com.piazuelo.oddjobs.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.fragments.HistoryEmployeeFragment;
import com.piazuelo.oddjobs.fragments.HistoryEmployerFragment;
import com.piazuelo.oddjobs.fragments.MyJobsFragment;
import com.piazuelo.oddjobs.fragments.PendingEmployeeFragment;
import com.piazuelo.oddjobs.fragments.PendingEmployerFragment;
import com.piazuelo.oddjobs.fragments.ShowJobsFragment;
import com.piazuelo.oddjobs.helpers.FirebaseHelper;
import com.piazuelo.oddjobs.helpers.ImageHelper;
import com.piazuelo.oddjobs.persistence.jobsDatabase;

import java.io.InputStream;
import java.util.ArrayList;

public class MainOddJobsActivity extends AppCompatActivity {

    public static final String TAG = "MAIN";

    private FirebaseHelper firebaseHelper;
    private Context mainContext;

    private String userEmail;
    public Drawer drawer;
    private Toolbar toolbar;

    private jobsDatabase databaseSQL;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databaseJobs = database.getReference("jobs");

    private boolean doingAsyncJobPushing;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseHelper = new FirebaseHelper();
        setContentView(R.layout.activity_main_odd_jobs);

        if(getIntent() != null && getIntent().hasExtra("email")) {
            userEmail = getIntent().getStringExtra("email");
        } else {
            userEmail ="no email received";
        }

        createFrameLayout();

        createDrawable();

        mainContext = this;

        databaseSQL = new jobsDatabase(this);
        doingAsyncJobPushing = false;
        aJobWithoutInternetWasCreated();

        super.onBackPressed();
    }

    public void createDrawable(){
        toolbar = (Toolbar) findViewById(R.id.toolbarMainOddJobs);
        toolbar.setTitleTextColor(Color.WHITE);
        if(getIntent() != null && getIntent().hasExtra("role")) {
            String role = getIntent().getStringExtra("role");

            if(toolbar != null){
                toolbar.setTitle("OddJob role: Employee");
            }

        }


        // Create the AccountHeader
        Drawable profilePic = getResources().getDrawable(R.drawable.male_icon);
        try {
            Uri pic = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
            Log.d(TAG, pic.toString());
            InputStream is = getContentResolver().openInputStream(pic);
            profilePic = Drawable.createFromStream(is, pic.toString());
            Log.d(TAG, "profile pic set");
        } catch(Exception e) {
            getResources().getDrawable(R.drawable.male_icon);
            Log.d(TAG, "Exception");
            Log.d(TAG, e.getMessage());
        }

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                //.withHeaderBackground(R.drawable.header_image)
                .withHeaderBackground(getScaledImage(ContextCompat.getDrawable(getApplicationContext(), R.drawable.header_image), 0.1f))
                .withOnlySmallProfileImagesVisible(false)
                .withThreeSmallProfileImages(false)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())
                                .withEmail(userEmail)
                                .withIcon(getScaledImage(profilePic, 0.1f)),
                        new ProfileDrawerItem()
                                .withName("Edit profile")
                                .withSelectable(false)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        editProfile();
                                        return false;
                                    }
                                })
                                .withIcon(R.drawable.edit),
                        new ProfileDrawerItem()
                                .withName("Sign out")
                                .withSelectable(false)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                                        mAuth.signOut();
                                        Intent i = new Intent(MainOddJobsActivity.this, LoginActivity.class);
                                        startActivity(i);
                                        finish();

                                        return false;
                                    }
                                })
                                .withIcon(R.drawable.signout)

                )
                .build();



        //items to create
        PrimaryDrawerItem item_As_OddEmployer = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_As_OddEmployer).withSelectable(false);
        PrimaryDrawerItem item_AcceptedEmployer = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_Accepted);
        item_AcceptedEmployer.withTextColor(Color.GRAY);
        item_AcceptedEmployer.withIcon(GoogleMaterial.Icon.gmd_assignment_turned_in);
        PrimaryDrawerItem item_outstandingEmployer  = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_outstanding );
        item_outstandingEmployer.withTextColor(Color.GRAY);
        item_outstandingEmployer.withIcon(FontAwesome.Icon.faw_calendar_check_o);
        PrimaryDrawerItem item_HistoryEmployer = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_History);
        item_HistoryEmployer.withTextColor(Color.GRAY);
        item_HistoryEmployer.withIcon(FontAwesome.Icon.faw_history);

        PrimaryDrawerItem item_As_OddEmployee = new PrimaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_As_OddEmployee).withSelectable(false);
        PrimaryDrawerItem item_OffersEmployee = new PrimaryDrawerItem().withIdentifier(7).withName(R.string.drawer_item_Offers);
        item_OffersEmployee.withTextColor(Color.GRAY);
        item_OffersEmployee.withIcon(GoogleMaterial.Icon.gmd_build);
        PrimaryDrawerItem item_outstandingEmployee  = new PrimaryDrawerItem().withIdentifier(8).withName(R.string.drawer_item_outstanding );
        item_outstandingEmployee.withTextColor(Color.GRAY);
        item_outstandingEmployee.withIcon(FontAwesome.Icon.faw_calendar_check_o);
        PrimaryDrawerItem item_HistoryEmployee = new PrimaryDrawerItem().withIdentifier(9).withName(R.string.drawer_item_History);
        item_HistoryEmployee.withTextColor(Color.GRAY);
        item_HistoryEmployee.withIcon(FontAwesome.Icon.faw_history);


        //create the drawer and remember the `Drawer` result object
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item_As_OddEmployer,
                        item_AcceptedEmployer,
                        item_outstandingEmployer,
                        item_HistoryEmployer,
                        new DividerDrawerItem(),
                        item_As_OddEmployee,
                        item_OffersEmployee,
                        item_outstandingEmployee,
                        item_HistoryEmployee,
                        new DividerDrawerItem()

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // starts in 1

                        //My jobs
                        if(position == 2){
                            showMyJobs();
                        }
                        if(position == 3){
                            showPendingEmployer();
                        }
                        if(position == 4){
                            showHistoryEmployer();
                        }

                        //Search jobs
                        if(position == 7){
                            showSearchJobs();
                        }
                        if(position == 8){
                            showPendingEmployee();
                        }
                        if(position == 9){
                            showHistoryEmployee();
                        }



                        try{Thread.sleep(10);}catch (Exception e){}

                        drawer.closeDrawer();
                        return true;
                    }
                })
                .build();

        drawer.setSelection(item_OffersEmployee);
    }

    private void editProfile() {
        Intent i = new Intent(this, ProfileActivity.class);
        startActivity(i);
    }

    public void createFrameLayout(){
        // Create a new Fragment to be placed in the activity layout
        Fragment showJobsFragment = new ShowJobsFragment();

        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, showJobsFragment).commit();

    }

    public Drawable getScaledImage(Drawable image, float scaleFactor) {
        if ((image == null) || !(image instanceof BitmapDrawable)) {
            return image;
        }

        Bitmap b = ((BitmapDrawable)image).getBitmap();

        int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
        int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);

        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);

        image = new BitmapDrawable(getResources(), bitmapResized);

        return image;
    }

    public void putFragmentInView(Fragment newFragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public void showMyJobs(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for the oddjobs you've created...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.getView().setLayoutParams(params);
        snack.show();
        putFragmentInView(new MyJobsFragment());
        //Modify the selected items in drawer and bottom
        if(drawer.getCurrentSelectedPosition() != 2){
            //drawer is not in show my jobs
            drawer.setSelectionAtPosition(2);
        }
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employer");
        }
    }
    public void showPendingEmployer(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for the oddjobs you've created...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.show();
        putFragmentInView(new PendingEmployerFragment());
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employer");
        }
    }
    public void showHistoryEmployer(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for the oddjobs you've created...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.show();
        putFragmentInView(new HistoryEmployerFragment());
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employer");
        }
    }
    public void showSearchJobs(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for your perfect oddjobs ...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.show();
        putFragmentInView(new ShowJobsFragment());
        //Modify the selected items in drawer and bottom
        if(drawer.getCurrentSelectedPosition() != 7){
            //drawer is not in search jobs
            drawer.setSelectionAtPosition(7);
        }
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employee");
        }
    }
    public void showPendingEmployee(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for your perfect oddjobs ...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.show();
        putFragmentInView(new PendingEmployeeFragment());
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employee");
        }

    }
    public void showHistoryEmployee(){
        Snackbar snack = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Searching for your perfect oddjobs ...", Snackbar.LENGTH_LONG);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                snack.getView().getLayoutParams();
        snack.show();
        putFragmentInView(new HistoryEmployeeFragment());
        if(toolbar != null){
            toolbar.setTitle("OddJob role: Employee");
        }
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    String jobsPushed= "";
    public void aJobWithoutInternetWasCreated(){
        jobsPushed= "";
        if(!doingAsyncJobPushing){
            doingAsyncJobPushing = true;
            new AsyncJobPush(mainContext).execute();

        }

    }
    public void publishJobsPushed(){
        if(!jobsPushed.equals("")){
            new AlertDialog.Builder(mainContext)
                    .setTitle("Perfect! You have internet")
                    .setMessage("Your jobs are visible to everyone:\n"+jobsPushed)
                    .create().show();
        }
    }

    public FirebaseHelper getFirebaseHelper() {
        return firebaseHelper;
    }

    public class AsyncJobPush extends AsyncTask<Void, Void, Void> {
        Context asyncContext;
        public AsyncJobPush(Context context){

            asyncContext = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            //Log.d(TAG, "ESTA HACIENDO LA TAREA EN BACKGROUND");
            while(!ConnectivityHelper.isOnline(asyncContext)){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Llega a encontrar trabajos SQL");

            ArrayList jobs = (ArrayList) databaseSQL.getAllJobs();
            for(int i=0;i< jobs.size();i++){
                Job current = (Job)jobs.get(i);

                System.out.println("Job found in data base: "+current.getName());

                databaseJobs.push().setValue(current);
                jobsPushed += i+1+". " +current.getName()+"\n ";

            }
            databaseSQL.deleteJobs();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            doingAsyncJobPushing = false;
            publishJobsPushed();
            super.onPostExecute(result);

        }
    }
}
