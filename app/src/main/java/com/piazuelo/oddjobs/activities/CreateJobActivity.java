package com.piazuelo.oddjobs.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.helpers.ImageHelper;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.persistence.jobsDatabase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class CreateJobActivity extends AppCompatActivity
        implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {

    private Uri imgToUpload;
    private ImageView clientImage;

    private static final int REQUEST_PHOTO = 123;
    private static final int REQUEST_PERMISSION = 124;

    private String selectedCategory;

    private Context mContext;

    private boolean clientUploadedAnImage;


    private boolean imageFinishUpload;

    private int yearS;
    private int montS;
    private int dayS;
    private int hourS;
    private int minuteS;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databaseJobs = database.getReference("jobs");
    StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();


    //________________________________________________________
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;


    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private static final String TAG = CreateJobActivity.class.getSimpleName();


    //__________________________________________________-

    private LatLng lastPointSelected;


    //____________________________________________________-

    private jobsDatabase databaseSQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseSQL = new jobsDatabase(this);

        setContentView(R.layout.activity_create_job);

        mContext = this;

        clientUploadedAnImage = false;

        clientImage = (ImageView) findViewById(R.id.imagePreview);

        Button selectImage = (Button) findViewById(R.id.createJobButtonSelectImages);
        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectivityHelper.isOnline(mContext)){
                    new AlertDialog.Builder(mContext)
                            .setTitle("Sorry")
                            .setMessage(R.string.ifnotonlinenoimage)
                            .setNegativeButton("Ok", null)
                            .create().show();
                }
                else{
                    selectImage();
                }

            }
        });

        final Button createJob = (Button) findViewById(R.id.createJobCreate);
        createJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJob();
            }
        });

        lastPointSelected = null;
        selectedCategory = "Home";
        imageFinishUpload = false;

        getLocationPermission();

        createMap();

        createSpinner();

        createCalendar();

        createTimePicker();

    }

    public void createTimePicker() {
        TimePicker pickerDate = (TimePicker) findViewById(R.id.tpHour);

        Calendar today = Calendar.getInstance();
        hourS = Calendar.HOUR_OF_DAY;
        minuteS = Calendar.MINUTE;

        pickerDate.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hourS = hourOfDay;
                minuteS = minute;
            }
        });

    }

    public void createCalendar() {
        DatePicker pickerDate = (DatePicker) findViewById(R.id.dpDate);

        Calendar today = Calendar.getInstance();
        yearS = Calendar.YEAR;
        montS = Calendar.MONTH;
        dayS = Calendar.DAY_OF_MONTH;

        pickerDate.init(
                today.get(Calendar.YEAR),
                today.get(Calendar.MONTH),
                today.get(Calendar.DAY_OF_MONTH) - 1,
                new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker view,
                                              int year, int monthOfYear, int dayOfMonth) {
                        Log.d(TAG, "date changed " + year + "/" + monthOfYear + "/" + dayOfMonth);
                        yearS = year;
                        montS = monthOfYear;
                        dayS = dayOfMonth;

                    }
                });

    }


    public void createJob() {
        EditText titleJob = (EditText) findViewById(R.id.createJobTitle);
        final String titleJ = titleJob.getText().toString();

        EditText descriptionJob = (EditText) findViewById(R.id.createJobDescription);
        final String description = descriptionJob.getText().toString();

        EditText paymentJob = (EditText) findViewById(R.id.createJobPayment);
        final String payment = paymentJob.getText().toString();

        String latitude = "no data";
        String longitude = "no data";
        if (lastPointSelected != null) {
            latitude = lastPointSelected.latitude + "";
            longitude = lastPointSelected.longitude + "";
        }
        final String finalLat = latitude;
        final String finalLong = longitude;


        EditText addressJob = (EditText) findViewById(R.id.createJobAddres);
        final String address = addressJob.getText().toString();

        if (titleJ.equals("") || titleJ.length() < 3) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Title")
                    .setMessage(R.string.title_not_empty)
                    .setNegativeButton("Ok", null)
                    .create().show();
            return;
        }
        if (description.equals("") || description.length() < 20) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Description")
                    .setMessage(R.string.description_not_empty)
                    .setNegativeButton("Ok", null)
                    .create().show();
            return;
        }
        if (payment.equals("") || Integer.parseInt(payment) < 100) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Payment")
                    .setMessage(R.string.payment_not_empty)
                    .setNegativeButton("Ok", null)
                    .create().show();
            return;
        }

        Calendar currentTime = Calendar.getInstance();

        int cyearS = currentTime.get(Calendar.YEAR);
        int cmontS = currentTime.get(Calendar.MONTH);
        int cdayS = currentTime.get(Calendar.DAY_OF_MONTH);
        int chourS = currentTime.get(Calendar.HOUR_OF_DAY);
        int cminuteS = currentTime.get(Calendar.MINUTE);

        currentTime.set(cyearS, cmontS, cdayS, chourS, cminuteS);

        final Calendar finalCal = Calendar.getInstance();
        finalCal.set(yearS, montS, dayS, hourS, minuteS);
        //System.out.println("Date of user:" + yearS + "-" + montS + "-" + dayS + "-" + hourS + "-" + minuteS);
        //System.out.println("Date current:" + cyearS + "-" + cmontS + "-" + cdayS + "-" + chourS + "-" + cminuteS);
        //System.out.println(finalCal.compareTo(currentTime) + " -> Esto tiene que ser mayor a 0 para que no salga aviso");
        if (finalCal.compareTo(currentTime) < 0) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Time")
                    .setMessage(R.string.timeNotCorrect)
                    .setNegativeButton("Ok", null)
                    .create().show();
            return;
        }


        if (address.equals("") || address.length() < 8) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Address")
                    .setMessage(R.string.address_not_empty)
                    .setNegativeButton("Ok", null)
                    .create().show();
            return;
        }

        new AlertDialog.Builder(mContext)
                .setTitle("Confirm")
                .setMessage(R.string.confirmCreateJob)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // POST A FIRE BASE
                        //System.out.println("post a fire base");

                        if(!ConnectivityHelper.isOnline(mContext)){
                            imageFinishUpload = true;

                        } else {
                            new AsyncLoader(mContext).execute();

                        }

                        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                        if (currentUser != null) {
                            final String url = "https://www.jainsusa.com/images/store/landscape/not-available.jpg";
                            //push image

                            if (!clientUploadedAnImage) {

                                final Job newJob = new Job(titleJ, description, Integer.parseInt(payment), new com.piazuelo.oddjobs.entities.Location(finalLat, finalLong), address, url, currentUser.getUid(), selectedCategory, finalCal.getTimeInMillis(), "NO_ASSIGNED_YET");
                                internetCheckBeforePush(newJob);
                            } else {
                                // Get the data from an ImageView as bytes
                                clientImage.setDrawingCacheEnabled(true);
                                clientImage.buildDrawingCache();
                                Bitmap bitmap = clientImage.getDrawingCache();
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                byte[] data = baos.toByteArray();

                                StorageReference storage = mStorageRef.child("jobs_pics/users/" + currentUser.getUid() + "/" + titleJ + new Date());

                                UploadTask uploadTask = storage.putBytes(data);
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                        //System.out.println("IMAGEN NO PUDO SUBIRLA");
                                        final Job newJob = new Job(titleJ, description, Integer.parseInt(payment), new com.piazuelo.oddjobs.entities.Location(finalLat, finalLong), address, url, currentUser.getUid(), (selectedCategory), finalCal.getTimeInMillis(), "NO_ASSIGNED_YET");
                                        internetCheckBeforePush(newJob);
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                        //System.out.println("IMAGEN SI PUDO SUBIRLA");
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        final Job newJob = new Job(titleJ, description, Integer.parseInt(payment), new com.piazuelo.oddjobs.entities.Location(finalLat, finalLong), address, downloadUrl.toString(), currentUser.getUid(), (selectedCategory), finalCal.getTimeInMillis(), "NO_ASSIGNED_YET");
                                        internetCheckBeforePush(newJob);
                                    }
                                });
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .create().show();
    }

    public void internetCheckBeforePush(final Job newJob){
        if (ConnectivityHelper.isOnline(mContext)) {
            pushJobToFirebase(newJob);
        } else {
            new AlertDialog.Builder(mContext)
                    .setTitle("No internet")
                    .setMessage(R.string.no_internet)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            databaseSQL.addJob(newJob);
                            imageFinishUpload = true;
                            Intent i = new Intent(mContext, MainOddJobsActivity.class);
                            startActivity(i);
                            finish();
                        }
                    })
                    .create().show();
        }

    }

    public  void pushJobToFirebase(final Job newJob) {
        //push job
        databaseJobs.push().setValue(newJob);
        imageFinishUpload = true;
        new AlertDialog.Builder(mContext)
                .setTitle("Perfect!")
                .setPositiveButton(R.string.job_created, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(mContext, MainOddJobsActivity.class);
                        startActivity(i);
                        finish();
                    }
                })
                .create().show();

    }


    private void createSpinner() {

        Spinner s = (Spinner) findViewById(R.id.SpinnerCategories);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        s.setAdapter(adapter);
        s.setOnItemSelectedListener(this);
    }


    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        String[] categories = getResources().getStringArray(R.array.categories_array);
        selectedCategory = categories[pos];
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Nothing
    }

    private void createMap() {

        if(mLocationPermissionGranted){

            // Construct a FusedLocationProviderClient.
            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

            // Build the map.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }



    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);

            super.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        System.out.println("el mapa es "+mMap);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //lstLatLngs.add(point);
                lastPointSelected = point;
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point));
            }
        });
        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mMap != null) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                            } else {
                                new AlertDialog.Builder(mContext)
                                        .setTitle("Ubication")
                                        .setMessage(R.string.gps_not_found)
                                        .setNegativeButton("Ok", null)
                                        .create().show();
                            }


                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            if(mMap != null){
                mMap.setMyLocationEnabled(true);
            }


        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                updateLocationUI();
            } else {

                //updateLocationUI();
            }
        }
    }

    /**
     * Prompts the user to select the current place from a list of likely places, and shows the
     * current place on the map - provided the user has granted location permission.
     */
    private void showCurrentPlace() {
        if (mMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {

        } else {
            // The user has not granted permission.
            Log.i(TAG, "The user did not grant location permission.");

            // Add a default marker, because the user hasn't selected a place.
            mMap.addMarker(new MarkerOptions()
                    .title(getString(R.string.default_info_title))
                    .position(mDefaultLocation)
                    .snippet(getString(R.string.default_info_snippet)));

            // Prompt the user for permission.
            getLocationPermission();
        }
    }
    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void selectImage() {
        PackageManager pm = getPackageManager();
        if(!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "This device don't have a camera", Toast.LENGTH_LONG).show();
            return;
        }

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "oddjobs/client.png");
        takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

        String pickTitle = "Take a picture or select one from your gallery";
        Intent chooser = Intent.createChooser(pickIntent, pickTitle);
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { takePicIntent });
        imgToUpload = Uri.fromFile(f);
        if(takePicIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(chooser, REQUEST_PHOTO);
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_PHOTO && resultCode == Activity.RESULT_OK) {
            if(data != null) {
                imgToUpload = data.getData();
            }
            if(imgToUpload != null){
                clientUploadedAnImage = true;
                Uri selectedImage = imgToUpload;
                getContentResolver().notifyChange(selectedImage, null);

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
                }
                ContentResolver cr = getContentResolver();
                Bitmap reducedSizeBitmap = ImageHelper.getBitmap(cr, ImageHelper.getRealPathFromURI(cr, imgToUpload));
                if(reducedSizeBitmap != null) {
                    clientImage.setImageBitmap(reducedSizeBitmap);
                }
            }else{
                Toast.makeText(this,"Error while capturing Image by Uri",Toast.LENGTH_LONG).show();
            }

        }
    }


    private class AsyncLoader extends AsyncTask<Void, Void, Void>{
        private ProgressDialog dialog;
        public AsyncLoader(Context context){
             dialog = new ProgressDialog(context); // this = YourActivity
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Uploading the OddJob. Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            while(!imageFinishUpload){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            //hide the dialog
            dialog.dismiss();
            super.onPostExecute(result);
        }
    }

}
