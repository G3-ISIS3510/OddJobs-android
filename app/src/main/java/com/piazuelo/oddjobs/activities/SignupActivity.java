package com.piazuelo.oddjobs.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.InputFormat;

public class SignupActivity extends AppCompatActivity {

    public static final String TAG = "SIGNUP";

    /**
     * Firsebase authentication service
     */
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;

    /**
     * UI components
     */
    private EditText email;
    private EditText name;
    private EditText firstPassword;
    private EditText secondPassword;
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        // UI components
        email = findViewById(R.id.emailSignUpTxt);
        name = findViewById(R.id.nameSignUpTxt);
        firstPassword = findViewById(R.id.password1SignUpTxt);
        secondPassword = findViewById(R.id.password2SignUpTxt);

        Button signUpBtn = findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                signUp(view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private String getEmail() {
        return email.getText() != null ? email.getText().toString().trim() : "";
    }

    private String getName() {
        return name.getText() != null ? name.getText().toString().trim() : "";
    }

    private String getFirstPassword() {
        return firstPassword.getText() != null ? firstPassword.getText().toString().trim() : "";
    }

    private String getSecondPassword() {
        return secondPassword != null ? secondPassword.getText().toString().trim() : "";
    }

    private void signUp(final View view) {
        String em = getEmail();
        String n = getName();
        String pw = getFirstPassword();
        String pw2 = getSecondPassword();
        if(em.isEmpty()) {
            Toast.makeText(this, R.string.noEmailProvided, Toast.LENGTH_LONG).show();
            return;
        }
        if(n.isEmpty()) {
            Toast.makeText(this, R.string.noNameProvided, Toast.LENGTH_LONG).show();
            return;
        }
        if(!InputFormat.validPassword(pw)) {
            Toast.makeText(this, R.string.invalidPassword, Toast.LENGTH_LONG).show();
            wipeUI(false, false, true);
            return;
        }
        if(!InputFormat.passwordsMatch(pw, pw2)) {
            Toast.makeText(this, R.string.differentPasswords, Toast.LENGTH_LONG).show();
            wipeUI(false, false, true);
            return;
        }

        loadingDialog = new ProgressDialog(SignupActivity.this);
        loadingDialog.setMessage(getApplicationContext().getString(R.string.creatingAccount));
        loadingDialog.setTitle(R.string.loading);
        loadingDialog.show();

        mAuth.createUserWithEmailAndPassword(em, pw)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(SignupActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            wipeUI(false, false, false);
                            if(loadingDialog != null)
                                loadingDialog.dismiss();
                            return;
                        }
                        saveUsersName(view);

                        if(loadingDialog != null)
                            loadingDialog.dismiss();
                    }
                });
    }

    private void saveUsersName(final View view) {
        final String userName = getName();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(userName)
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                            Toast.makeText(SignupActivity.this, R.string.success, Toast.LENGTH_SHORT).show();
                            // Move to next activity
                            mDatabase.child("users").child(user.getUid()).child("name").setValue(userName);
                            Intent i = new Intent(view.getContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(SignupActivity.this, R.string.somethingWrong, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void wipeUI(boolean e, boolean fp, boolean sp) {
        if(e) email.setText("");
        if(fp) firstPassword.setText("");
        if(sp) secondPassword.setText("");
    }

}
