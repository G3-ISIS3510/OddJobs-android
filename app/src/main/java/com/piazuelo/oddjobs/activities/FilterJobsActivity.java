package com.piazuelo.oddjobs.activities;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.AccelerometerHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterJobsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, SensorEventListener {

    public static final String TAG = "FILTERS";
    public static final int RESULT_CODE = 2;

    private SensorManager mSensorManager;
    private Sensor mAcc;

    /**
     * UI components
     */
    private EditText minReward;
    private EditText maxReward;
    private EditText nameQuery;
    private Spinner typeQuery;

    private List<CharSequence> categories;
    private int selectedCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_jobs);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        minReward = findViewById(R.id.minRewardEditTxt);
        maxReward = findViewById(R.id.maxRewardEditTxt);
        nameQuery = findViewById(R.id.nameFilterEditTxt);
        typeQuery = findViewById(R.id.typeFilterSpinner);
        createSpinner();

        Intent i = getIntent();
        if(i.hasExtra("minReward")) {
            String text = "" + i.getDoubleExtra("minReward", 0);
            minReward.setText(text);
        }
        if(i.hasExtra("maxReward")) {
            String text = "" + i.getDoubleExtra("maxReward", 0);
            maxReward.setText(text);
        }
        if(i.hasExtra("name"))
            nameQuery.setText(i.getStringExtra("name"));

        categories = getCategories();
        selectedCategory = -1;

        Button clearBtn = findViewById(R.id.clearFiltersBtn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFilters();
                Toast.makeText(FilterJobsActivity.this, R.string.filtersReverted, Toast.LENGTH_SHORT).show();
            }
        });

        Button saveBtn = findViewById(R.id.saveFiltersBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFilters();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    private void createSpinner() {
        List<CharSequence> categories = getCategories();
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.categories_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        typeQuery.setAdapter(adapter);
        typeQuery.setOnItemSelectedListener(this);
    }

    private List<CharSequence> getCategories() {
        String[] array = getResources().getStringArray(R.array.categories_array);
        List<CharSequence> categories = new ArrayList<>();
        categories.add("");
        categories.addAll(Arrays.asList(array));
        return categories;
    }

    private void clearFilters() {
        minReward.setText("");
        maxReward.setText("");
        nameQuery.setText("");
        selectedCategory = -1;
        typeQuery.setSelection(0);
    }

    private void saveFilters() {
        Intent result = new Intent();

        String minRText = extractText(minReward);
        String maxRText = extractText(maxReward);
        if(!minRText.equals("") && !maxRText.equals("") && Double.parseDouble(minRText) > Double.parseDouble(maxRText)) {
            Toast.makeText(FilterJobsActivity.this, R.string.invalidRewardInterval, Toast.LENGTH_LONG).show();
            return;
        }
        if (!minRText.equals("")) {
            double minR = Double.parseDouble(minRText);
            if(minR < 0) {
                Toast.makeText(FilterJobsActivity.this, R.string.positiveNumbers, Toast.LENGTH_LONG).show();
                return;
            }
            result.putExtra("minReward", minR);
        }
        if (!maxRText.equals("")) {
            double maxR = Double.parseDouble(maxRText);
            if(maxR < 0) {
                Toast.makeText(FilterJobsActivity.this, R.string.positiveNumbers, Toast.LENGTH_LONG).show();
                return;
            }
            result.putExtra("maxReward", maxR);
        }
        String n = extractText(nameQuery);
        result.putExtra("name", n);
        String category = selectedCategory != -1 ? categories.get(selectedCategory).toString() : "";
        result.putExtra("type", category);

        setResult(RESULT_CODE, result);
        finish();
    }

    private String extractText(EditText v) {
        return v.getText() != null ? v.getText().toString().trim() : "";
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCategory = i;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        selectedCategory = -1;
    }


    private long mShakeTimestamp;
    private int mShakeCount;
    @Override
    public void onSensorChanged(SensorEvent event) {
        double gForce = AccelerometerHelper.getGforce(event);
        if (gForce > AccelerometerHelper.SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + AccelerometerHelper.SHAKE_SLOP_TIME_MS > now) {
                return;
            }
            // reset the shake count after 3 seconds of no shakes
            if (mShakeTimestamp + AccelerometerHelper.SHAKE_COUNT_RESET_TIME_MS < now) {
                mShakeCount = 0;
            }
            mShakeTimestamp = now;
            mShakeCount++;
            clearFilters();
            saveFilters();
            Toast.makeText(this, R.string.filtersCleared, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
