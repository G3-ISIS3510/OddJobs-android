package com.piazuelo.oddjobs.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LOGIN";
    private static final int RC_SIGN_IN = 0;

    /**
     * Firsebase authentication service
     */
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    /**
     * Google authentication service
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Facebook authentication service
     */
    private CallbackManager mCallbackManager;

    // UI references.
    private EditText emailView;
    private EditText passwordView;
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // UI components
        emailView = findViewById(R.id.emailEditTxt);
        passwordView = findViewById(R.id.passwordEditTxt);

        Button signInBtn = findViewById(R.id.signInBtn);
        SignInButton signInGoogleBtn = findViewById(R.id.googleSignInBtn);
        final LoginButton signInFacebookBtn = findViewById(R.id.facebookSignInBtn);
        TextView signUpTxt = findViewById(R.id.signUpTxt);
        FrameLayout fbContainer = findViewById(R.id.facebookBtnContainer);

        signUpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open sign up activity
                goToSignUp();
            }
        });

        // Firebase authentication
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Sign in user
                String email  = getEmail();
                String passw = getPassword();
                if(email.isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.noEmailProvided, Toast.LENGTH_LONG).show();
                    return;
                }
                if(passw.isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.noPasswordProvided, Toast.LENGTH_LONG).show();
                    return;
                }
                if(!ConnectivityHelper.isOnline(getApplicationContext())) {
                    Toast.makeText(LoginActivity.this, R.string.noConnection, Toast.LENGTH_LONG).show();
                    return;
                }
                signIn(email, passw);
            }
        });

        // Google authentication
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(
                        this,
                        new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                // An unresolvable error has occurred and Google APIs (including Sign-In) will not
                                // be available.
                                Log.d(TAG, "onConnectionFailed:" + connectionResult);
                                Toast.makeText(LoginActivity.this, R.string.googlePlayError, Toast.LENGTH_SHORT).show();
                            }
                        }
                )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleSignIn();
            }
        });


        // Facebook authentication
        fbContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!ConnectivityHelper.isOnline(getApplicationContext())) {
                    Toast.makeText(LoginActivity.this, R.string.noConnection, Toast.LENGTH_LONG).show();
                } else {
                    signInFacebookBtn.performClick();
                }
            }
        });

        mCallbackManager = CallbackManager.Factory.create();

        signInFacebookBtn.setReadPermissions("email", "public_profile");
        signInFacebookBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                Toast.makeText(LoginActivity.this, R.string.facebookCancelled, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                if(ConnectivityHelper.isOnline(getApplicationContext())) {
                    Toast.makeText(LoginActivity.this, R.string.facebookFailed, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.noConnection, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            next();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private String getEmail() {
        return emailView.getText() != null ? emailView.getText().toString().trim() : "";
    }

    private String getPassword() {
        return passwordView.getText() != null ? passwordView.getText().toString().trim() : "";
    }

    private void signIn(String email, String password) {
        loadingDialog = new ProgressDialog(LoginActivity.this);
        loadingDialog.setMessage(getString(R.string.loggingIn));
        loadingDialog.setTitle(R.string.loading);
        loadingDialog.show();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        if(loadingDialog != null){
                            loadingDialog.dismiss();
                        }

                        // If sign in fails, display a message to the user.
                        if(task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, R.string.authenticationSuccessful, Toast.LENGTH_SHORT).show();
                            next();
                        }
                        else {
                            Log.w(TAG, "signInWithEmail:failed ");
                            Toast.makeText(LoginActivity.this, R.string.authenticationFailed, Toast.LENGTH_SHORT).show();
                        }
                        // If sign in succeeds the auth state listener will be notified and logic to
                        // handle the signed in user can be handled in the listener.
                    }
                });
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (data != null && requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed
                if(!ConnectivityHelper.isOnline(getApplicationContext())) {
                    Toast.makeText(LoginActivity.this, R.string.noConnection, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.googleAuthFailure, Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getIdToken());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            Toast.makeText(LoginActivity.this, R.string.authenticationSuccessful, Toast.LENGTH_SHORT).show();
                            next();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {

        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");
                    next();
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void goToSignUp() {
        Intent i = new Intent(LoginActivity.this, SignupActivity.class);
        startActivity(i);
    }

    private void next() {
        FirebaseUser user = mAuth.getCurrentUser();
        Intent i = new Intent(LoginActivity.this, MainOddJobsActivity.class);
        i.putExtra("email", user.getEmail());
        i.putExtra("role", "employee");
        startActivity(i);
        finish();
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.backAgain, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}

