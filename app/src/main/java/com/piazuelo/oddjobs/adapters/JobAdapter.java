package com.piazuelo.oddjobs.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.helpers.InputFormat;
import com.piazuelo.oddjobs.entities.Job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class JobAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private List<Job> jobs;

    public JobAdapter(LayoutInflater li, List<Job> jobs) {
        this.jobs = jobs;
        Collections.sort(this.jobs, new JobComparator());
        inflater = li;
    }

    @Override
    public int getCount() {
        return jobs.size();
    }

    @Override
    public Object getItem(int i) {
        return jobs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if (view == null)
            view = inflater.inflate(R.layout.job_preview, null);

        TextView jobName = view.findViewById(R.id.jobPreviewNameTxt);
        TextView jobLocation = view.findViewById(R.id.jobPreviewLocationTxt);
        TextView jobReward = view.findViewById(R.id.jobPreviewRewardTxt);
        TextView jobDate = view.findViewById(R.id.jobPreviewDateTxt);

        Job j = jobs.get(i);

        jobName.setText(j.getType()+":  "+j.getName());
        // TODO : Add distance to location
        jobLocation.setText(j.getAddress());
        String reward = InputFormat.formatCurrency(j.getReward());
        jobReward.setText(reward.split("\\.")[0]);

        long millis = j.getDate();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(millis);
        String dateToShow = date.get(date.DAY_OF_MONTH)+"/"+
                (date.get(date.MONTH)+1)+"/"+
                            date.get(date.YEAR)+" "+
                            showDigit(date.get(date.HOUR))+":"+
                            showDigit(date.get(date.MINUTE));

        jobDate.setText(dateToShow);

        return view;
    }

    private String showDigit(int digit){
        String digitS = digit+"";
        if(digitS.length() == 1){
            return "0"+digitS;
        }
        return digitS;
    }

    public void updateData(ArrayList<Job> newJobs) {
        this.jobs = newJobs;
        notifyDataSetChanged();
    }

    public void sort() {
        Collections.sort(this.jobs, new JobComparator());
    }

    public class JobComparator implements Comparator<Job> {
        @Override
        public int compare(Job j1, Job j2) {
            if(j1.getDate() < j2.getDate())
                return -1;
            else if(j1.getDate() > j2.getDate())
                return 1;
            else {
                if(j1.getReward() < j2.getReward())
                    return -1;
                else if(j1.getReward() > j2.getReward())
                    return 1;
                else
                    return 0;
            }
        }
    }
}