package com.piazuelo.oddjobs.adapters;

/**
 * Created by jgtamura on 10/9/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.entities.User;

import java.util.ArrayList;
import java.util.List;


public class EmployeeAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private List<String> employees;
    private Context context;


    public EmployeeAdapter(Context cont, List<String> employees) {
        this.employees = employees;
        context = cont;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return employees.size();
    }

    @Override
    public Object getItem(int i) {
        return employees.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if (view == null)
            view = inflater.inflate(R.layout.employee_preview, null);



        String j = employees.get(i);
        System.out.println("USER: "+j);

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        final View finalView = view;
        mDatabase.child("users").child(j).child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.getValue(String.class);
                TextView empĺoyeeName = finalView.findViewById(R.id.employeePreviewNameTxt);
                empĺoyeeName.setText(name);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        mDatabase.child("users").child(j).child("rating").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long rating = dataSnapshot.getValue(Long.class);
                TextView empĺoyeeRating = finalView.findViewById(R.id.userRatingEmployee);
                empĺoyeeRating.setText(rating+"");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        mDatabase.child("users").child(j).child("reviews").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long reviews = dataSnapshot.getValue(Long.class);
                TextView empĺoyeeReviews = finalView.findViewById(R.id.employeePreviewReviewsTxt);
                empĺoyeeReviews.setText("   out of "+reviews+" reviews");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });





        return view;
    }
    public void updateData(ArrayList<String> employees) {
        this.employees = employees;
        notifyDataSetChanged();
    }
}