package com.piazuelo.oddjobs.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.activities.FilterJobsActivity;
import com.piazuelo.oddjobs.activities.JobDetailsActivity;
import com.piazuelo.oddjobs.activities.MainOddJobsActivity;
import com.piazuelo.oddjobs.adapters.JobAdapter;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.helpers.FirebaseHelper;

import java.util.ArrayList;

public class ShowJobsFragment extends Fragment {

    public static final String TAG = "SHOW_JOBS";
    public static final int FILTERS_REQUEST = 1;
    public static final int FILTERS_SUCCESS = 2;

    private ArrayList<Job> jobs;
    private static JobAdapter adapter;
    private static FirebaseHelper firebaseHelper;

    /**
     * Filter values
     */
    private double minReward = -1;
    private double maxReward = -1;
    private String nameQuery = "";
    private String typeQuery = "";

    public ShowJobsFragment() {
        // Required empty public constructor
    }

    public static ShowJobsFragment newInstance() {
        return new ShowJobsFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_jobs, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseHelper = ((MainOddJobsActivity)getActivity()).getFirebaseHelper();
        jobs = (ArrayList<Job>) firebaseHelper.getJobSearch();
        adapter = new JobAdapter((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE), jobs);

        ListView lv = view.findViewById(R.id.searchOtherJobsListView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Job j = (Job) adapter.getItem(i);
                String jKey = firebaseHelper.getJobKey(j);
                showDetails(j, jKey);
            }
        });

        FloatingActionButton filtersFab = view.findViewById(R.id.filtersFab);
        filtersFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), FilterJobsActivity.class);
                if(minReward != -1)
                    intent.putExtra("minReward", minReward);
                if(maxReward != -1)
                    intent.putExtra("maxReward", maxReward);
                if(!nameQuery.equals(""))
                    intent.putExtra("name", nameQuery);
                startActivityForResult(intent, FILTERS_REQUEST);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private boolean passesFilters(Job j) {
        if(minReward != -1 && j.getReward() < minReward) {
            Log.d(TAG, "No pasa porque es menor al mínimo");
            return false;
        }
        if(maxReward != -1 && j.getReward() > maxReward) {
            Log.d(TAG, "No pasa porque es mayor al maximo");
            return false;
        }
        if(!j.getName().toLowerCase().contains(nameQuery.toLowerCase())) {
            Log.d(TAG, "No pasa por el nombre");
            return false;
        }
        if(!j.getType().contains(typeQuery)) {
            Log.d(TAG, "No pasa por el tipo");
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILTERS_REQUEST) {
            if(resultCode == FILTERS_SUCCESS) {
                if(data.hasExtra("minReward"))
                    minReward = data.getDoubleExtra("minReward", -1);
                else
                    minReward = -1;
                if(data.hasExtra("maxReward"))
                    maxReward = data.getDoubleExtra("maxReward", -1);
                else
                    maxReward = -1;
                nameQuery = data.getStringExtra("name");
                typeQuery = data.getStringExtra("type");
                Log.d(TAG, typeQuery);
            }
        }
        adapter.updateData(filterJobs());
    }

    private ArrayList<Job> filterJobs() {
        ArrayList<Job> newJobs = new ArrayList<>();
        for(Job j : jobs) {
            if(passesFilters(j)) {
                newJobs.add(j);
            }
        }
        return newJobs;
    }

    private void showDetails(Job j, String key) {
        Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
        intent.putExtra("key", key);
        intent.putExtra("job", j);
        startActivity(intent);
    }

    public static void dataChanged() {
        if(adapter != null){
            adapter.sort();
            adapter.notifyDataSetChanged();
        }
    }
}
