package com.piazuelo.oddjobs.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.activities.CreateJobActivity;
import com.piazuelo.oddjobs.activities.JobDetailsActivity;
import com.piazuelo.oddjobs.activities.MainOddJobsActivity;
import com.piazuelo.oddjobs.adapters.JobAdapter;
import com.piazuelo.oddjobs.entities.User;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.helpers.FirebaseHelper;

import java.util.ArrayList;

public class HistoryEmployerFragment extends Fragment {

    private FirebaseHelper firebaseHelper;
    private ArrayList<Job> jobs;
    private static JobAdapter adapter;


    public HistoryEmployerFragment() {
        // Required empty public constructor
    }

    public static HistoryEmployerFragment newInstance() {
        HistoryEmployerFragment fragment = new HistoryEmployerFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_employer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseHelper = ((MainOddJobsActivity)getActivity()).getFirebaseHelper();
        jobs = (ArrayList<Job>) firebaseHelper.getEmployerHistory();
        adapter = new JobAdapter((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE), jobs);

        ListView lv = view.findViewById(R.id.listViewMyJobsHistory);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Job j = (Job) adapter.getItem(i);
                if(j.getType().equals("FINISHED")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Finished");
                    alertDialog.setMessage("You have already rated your worker. This job has been finalized");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    final Dialog ratingDialog = new Dialog(getActivity());
                    final View dialogView = getLayoutInflater().inflate(R.layout.rate_employee, null);
                    Button cancelBtn = dialogView.findViewById(R.id.cancelRatingBtn);
                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ratingDialog.dismiss();
                        }
                    });
                    Button submitBtn = dialogView.findViewById(R.id.submitRatingBtn);
                    submitBtn.setOnClickListener(new AdapterView.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            RatingBar ratingBar = dialogView.findViewById(R.id.employeeRatingBar);
                            double rating = ratingBar.getRating();
                            submitRating(rating, j.getEmployeeId(), firebaseHelper.getJobKey(j));
                            ratingDialog.dismiss();
                        }
                    });
                    ratingDialog.setContentView(dialogView);
                    ratingDialog.show();
                }
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View thisView = view;
                if(ConnectivityHelper.isOnline(view.getContext())){
                    Intent i = new Intent(view.getContext(), CreateJobActivity.class);
                    startActivity(i);
                } else {
                    new AlertDialog.Builder(view.getContext())
                            .setTitle("No internet")
                            .setMessage(R.string.no_internet)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(thisView.getContext(), CreateJobActivity.class);
                                    startActivity(i);
                                    ((MainOddJobsActivity)getActivity()).aJobWithoutInternetWasCreated();
                                }
                            })
                            .create().show();
                }
            }
        });
    }

    private void submitRating(final double rating, final String employeeId, final String jobId) {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").child(employeeId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final User user = dataSnapshot.getValue(User.class);
                double newRating = ((user.getRating() * user.getReviews()) + rating) / (user.getReviews() + 1);
                mDatabase.getDatabase().getReference("users").child(employeeId).child("rating").setValue(newRating);
                mDatabase.getDatabase().getReference("users").child(employeeId).child("reviews").setValue(user.getReviews() + 1);
                mDatabase.getDatabase().getReference("jobs").child(jobId).child("type").setValue("FINISHED");
                Log.d("RATING", employeeId + " " + user.getRating() + " now " + newRating);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static void dataChanged() {
        if(adapter != null){
            adapter.sort();
            adapter.notifyDataSetChanged();
        }
    }
}
