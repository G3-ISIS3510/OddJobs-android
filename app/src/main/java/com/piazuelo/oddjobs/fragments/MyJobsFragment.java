package com.piazuelo.oddjobs.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.activities.CreateJobActivity;
import com.piazuelo.oddjobs.activities.JobDetailsActivity;
import com.piazuelo.oddjobs.activities.MainOddJobsActivity;
import com.piazuelo.oddjobs.adapters.JobAdapter;
import com.piazuelo.oddjobs.helpers.ConnectivityHelper;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.helpers.FirebaseHelper;

import java.util.ArrayList;

public class MyJobsFragment extends Fragment {

    private FirebaseHelper firebaseHelper;
    private ArrayList<Job> jobs;
    private static JobAdapter adapter;

    public MyJobsFragment() {
        // Required empty public constructor
    }

    public static MyJobsFragment newInstance() {
        return new MyJobsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_jobs, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseHelper = ((MainOddJobsActivity)getActivity()).getFirebaseHelper();
        jobs = (ArrayList<Job>) firebaseHelper.getEmployerAccepted();
        adapter = new JobAdapter((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE), jobs);

        ListView lv = view.findViewById(R.id.listViewMyJobs);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Job j = (Job) adapter.getItem(i);
                String jKey = firebaseHelper.getJobKey(j);
                showDetails(j, jKey);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View thisView = view;
                if( ConnectivityHelper.isOnline(view.getContext())){
                    Intent i = new Intent(view.getContext(), CreateJobActivity.class);
                    startActivity(i);
                }
                else{
                    new AlertDialog.Builder(view.getContext())
                            .setTitle("No internet")
                            .setMessage(R.string.no_internet)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(thisView.getContext(), CreateJobActivity.class);
                                    startActivity(i);
                                    ((MainOddJobsActivity)getActivity()).aJobWithoutInternetWasCreated();
                                }
                            })
                            .create().show();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showDetails(Job j, String key) {
        Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
        intent.putExtra("key", key);
        intent.putExtra("job", j);
        startActivity(intent);
    }

    public static void dataChanged() {
        if(adapter != null){
            adapter.sort();
            adapter.notifyDataSetChanged();
        }
    }
}
