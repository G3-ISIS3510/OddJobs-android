package com.piazuelo.oddjobs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.piazuelo.oddjobs.R;
import com.piazuelo.oddjobs.activities.MainOddJobsActivity;
import com.piazuelo.oddjobs.adapters.JobAdapter;
import com.piazuelo.oddjobs.entities.Job;
import com.piazuelo.oddjobs.helpers.FirebaseHelper;

import java.util.ArrayList;

public class HistoryEmployeeFragment extends Fragment {

    private FirebaseHelper firebaseHelper;
    private ArrayList<Job> jobs;
    private static JobAdapter adapter;

    public HistoryEmployeeFragment() {
        // Required empty public constructor
    }

    public static HistoryEmployeeFragment newInstance() {
        HistoryEmployeeFragment fragment = new HistoryEmployeeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_employee, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseHelper = ((MainOddJobsActivity)getActivity()).getFirebaseHelper();
        jobs = (ArrayList<Job>) firebaseHelper.getEmployeeHistory();
        adapter = new JobAdapter((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE), jobs);

        ListView lv = view.findViewById(R.id.listViewMyJobsHistoryEmployee);
        lv.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static void dataChanged() {
        if(adapter != null){
            adapter.sort();
            adapter.notifyDataSetChanged();
        }

    }
}
